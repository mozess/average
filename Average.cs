﻿using System;
using System.Linq;

namespace Average
{
    public class Average<T> where T : struct
    {
        readonly static Type[] allowedTypes = new Type[] { typeof(decimal), typeof(float), typeof(double), typeof(ulong), typeof(long), typeof(uint), typeof(int), typeof(sbyte), typeof(byte) };

        double summa = 0;
        double count = 0;

        public Average()
        {
            T a = default(T);
            if (!allowedTypes.Contains(a.GetType()))
                throw new ArgumentException("Недопустимый тип аргумента");
        }

        public void add(T value)
        {
            summa += Convert.ToDouble((dynamic)value);
            count++;
        }

        public T average()
        {
            if (count == 0)
            {
                return default(T);
            }
            else
            {
                var result = summa / count;
                return Convert.ChangeType(result, (dynamic)default(T).GetType());
            }
        }

        public void reset()
        {
            summa = 0;
            count = 0;
        }
    }
}
